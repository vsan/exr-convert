
#include "image_tools.h"

int main(int argc, char *argv[])
{
  std::string in_file = std::string(argv[1]);
  std::string out_file = std::string(argv[2]); //"/home/vs/107_4.raw"

  depth_data z, depth;
  readEXRToRawDepth(z, in_file, out_file);

//  readRawDepth(out_file, depth, true);
//
//  saveRawDepthToEXR(depth, "test.exr");

  return 0;
}
