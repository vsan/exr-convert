//
// Created by vsan on 18.04.19.
//

#include <cstring>
#include <iostream>
#include <fstream>
#include "image_tools.h"
#include "tinyexr.h"


bool saveRawDepthToEXR(depth_data &raw_depth, const std::string &path)
{
  EXRHeader header;
  InitEXRHeader(&header);

  EXRImage image;
  InitEXRImage(&image);

  float* image_ptr[1];
  image_ptr[0] = &(raw_depth.data.at(0)); // raw_depth.data.data()

  image.num_channels = 1;
  image.images = (unsigned char**)image_ptr;
  image.width = raw_depth.w;
  image.height = raw_depth.h;

  header.num_channels = 1;
  header.channels = (EXRChannelInfo *)malloc(sizeof(EXRChannelInfo) * header.num_channels);
  // Must be BGR(A) order, since most of EXR viewers expect this channel order.
  strncpy(header.channels[0].name, "z", 255); header.channels[0].name[strlen("z")] = '\0';

  header.pixel_types = (int *)malloc(sizeof(int) * header.num_channels);
  header.requested_pixel_types = (int *)malloc(sizeof(int) * header.num_channels);
  for (int i = 0; i < header.num_channels; i++)
  {
    header.pixel_types[i] = TINYEXR_PIXELTYPE_FLOAT; // pixel type of input image
    header.requested_pixel_types[i] = TINYEXR_PIXELTYPE_FLOAT; // pixel type of output image to be stored in .EXR
  }

  const char* err;
  int ret = SaveEXRImageToFile(&image, &header, path.c_str(), &err);
  if (ret != TINYEXR_SUCCESS)
  {
    std::cerr << "Save EXR error: " << err << std::endl;
    return false;
  }
  std::cout << "Saved exr file: " << path.c_str() << std::endl;


  free(header.channels);
  free(header.pixel_types);
  free(header.requested_pixel_types);

}


bool readEXRToRawDepth(depth_data &raw_depth, const std::string &path, const std::string &out_path)
{
  float* out; // width * height * RGBA
  int width;
  int height;
  const char* err = nullptr; // or nullptr in C++11

  int ret = LoadEXR(&out, &width, &height, path.c_str(), &err);

  if (ret != TINYEXR_SUCCESS)
  {
    if (err)
    {
      fprintf(stderr, "ERR : %s\n", err);
      std::cerr << "ERR : " << err << std::endl;
      delete err;
    }
  }
  else
  {
    raw_depth.data.resize(width * height);
    raw_depth.w = uint32_t(width);
    raw_depth.h = uint32_t(height);

    int j = 0;
    for (int i = 0; i < width * height * 4; i += 4)
    {
         raw_depth.data[j] = out[i];
         j++;
    }

  //  memcpy(raw_depth.data.data(), out, width * height * sizeof(float));

    free(out);
  }

  std::ofstream outfile(out_path, std::ios::binary);
  outfile.write((char*)&raw_depth.w, sizeof(uint32_t));
  outfile.write((char*)&raw_depth.h, sizeof(uint32_t));
  outfile.write((char*)raw_depth.data.data(), sizeof(float) * raw_depth.w * raw_depth.h);
  outfile.flush();
  outfile.close();

  return true;
}

bool readRawDepth(const std::string &filepath, depth_data &depth, bool debug_log)
{
  std::ifstream fin(filepath.c_str(), std::ios::binary);

  if (!fin.is_open())
  {
    std::cout << "Failed to open raw depth file at: " << filepath << std::endl;
    return false;
  }

  fin.read((char*)&depth.w, sizeof(uint32_t));
  fin.read((char*)&depth.h, sizeof(uint32_t));

  uint32_t dataSizeInBytes = sizeof(float) * depth.w * depth.h;

  if(debug_log)
  {
    std::cout << "Read depth file " << filepath << "; w = " << depth.w << ", h = " << depth.h << std::endl;
    std::cout << "Data size: " << dataSizeInBytes << " bytes" << std::endl;
  }

  depth.data.resize(depth.w * depth.h);
  fin.read((char*)depth.data.data(), dataSizeInBytes);

  if(debug_log)
  {
    std::cout << "Read " << depth.data.size() << " depth values. First 16 values read: " << std::endl;
    for (int i = 0; i < 16; ++i)
    {
      std::cout << depth.data[i] << " ";
    }
    std::cout << std::endl;
  }

  fin.close();
}
