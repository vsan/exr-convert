#ifndef DEPTH_EXP_IMAGE_TOOLS_H
#define DEPTH_EXP_IMAGE_TOOLS_H

#include <string>
#include <vector>

struct depth_data
{
    uint32_t w;
    uint32_t h;
    std::vector<float> data; //row-major
};

bool saveRawDepthToEXR(depth_data &raw_depth, const std::string &path);
bool readRawDepth(const std::string &filepath, depth_data &depth, bool debug_log = false);

bool readEXRToRawDepth(depth_data &raw_depth, const std::string &path, const std::string &out_path);

#endif //DEPTH_EXP_IMAGE_TOOLS_H
